/* 
 Manchester Decoding, reading by delay rather than interrupt
 Rob Ward August 2014
 This example code is in the public domain.
 Use at your own risk, I will take no responsibility for any loss whatsoever its deployment.
 Visit https://github.com/robwlakes/433MHz_Tx_Rx 
 for the latest version and documentation. 

 Modified by Roman Svechnikov to use a light sensor as receiver
 */  

//Interface Definitions
int     RxPin      = A0;   //The number of signal from the Rx
int     ledPin     = 13;   //The number of the onboard LED pin

// Variables for Manchester Receiver Logic:
word    sDelay     = 50;   //Small Delay about 1/4 of bit duration (100 uS for analog read excluded)
word    lDelay     = 200;  //Long Delay about 1/2 of bit duration (100 uS for analog read excluded)
byte    polarity   = 0;    //0 for lo->hi==1 or 1 for hi->lo==1 for Polarity, sets tempBit at start
byte    tempBit    = 1;    //Reflects the required transition polarity
byte    discards   = 1;    //how many leading "bits" need to be dumped, usually just a zero if anything eg discards=1
byte    discNos    = 0;    //counter of how many discarded
boolean firstZero  = false;//has it processed the first zero yet?  This a "sync" bit.
boolean noErrors   = true; //flags if signal does not follow Manchester conventions

//variables for Header detection
byte    headerBits = 10;   //The number of ones expected to make a valid header
byte    headerHits = 0;    //Counts the number of "1"s to determine a header

//Variables for Byte storage
byte    dataByte   = 0;    //Accumulates the bit information
byte    nosBits    = 0;    //Counts to 8 bits within a dataByte
byte    maxBytes   = 8;    //Set the bytes collected after each header. NB if set too high, any end noise will cause an error
byte    nosBytes   = 0;    //Counter stays within 0 -> maxBytes

byte manchester[20];
int zeroLight = 0;

void setup() {
  Serial.begin(115200);
  zeroLight = analogRead(RxPin);
  pinMode(RxPin, INPUT);
  pinMode(ledPin, OUTPUT);
  Serial.println("Debug Manchester Rx+Tx");
  lDelay = 2 * sDelay;
  Serial.print("Using a delay of 1/4 bitWaveform ");
  Serial.print(sDelay + 100, DEC);
  Serial.print(" uSecs 1/2 bitWaveform ");
  Serial.print(lDelay + 100, DEC);
  Serial.println(" uSecs ");
  if (polarity){
    Serial.println("Negative Polarity hi->lo=1"); 
  }
  else{
    Serial.println("Positive Polarity lo->hi=1"); 
  }
  Serial.print(headerBits,DEC); 
  Serial.println(" bits expected for a valid header"); 
  if (discards){
    Serial.print(discards,DEC);
    Serial.println(" leading bits discarded from Packet"); 
  }
  else{
    Serial.println("All bits inside the Packet"); 
  }
  Serial.println("D 00 00001111 01 22223333 02 44445555 03 66667777 04 88889999 05 AAAABBBB 06 CCCCDDDD 07 EEEEFFFF 08 00001111 90 22223333"); 
  eraseManchester();
}

bool myRead(int pin) {
    // ~ 100 uS
    return !(analogRead(A0) < zeroLight - 50);
}

// Main routines, find header, then sync in with it, get a packet, and decode data in it, plus report any errors.
void loop(){
  tempBit = polarity ^ 1;
  noErrors = true;
  firstZero = false;
  headerHits = 0;
  nosBits = 0;
  nosBytes = 0;
  discNos = discards;

  while (noErrors && (nosBytes < maxBytes)){
    while(myRead(RxPin) != tempBit){
    }
    delayMicroseconds(sDelay);
    digitalWrite(ledPin, 0); 
    if (myRead(RxPin) != tempBit){
      noErrors=false;
    }
    else{
      byte bitState = tempBit ^ polarity;
      delayMicroseconds(lDelay);
      if(myRead(RxPin) == tempBit){ 
        tempBit = tempBit ^ 1;
      }
      if(bitState == 1){ 
        if(!firstZero){
          headerHits++;
          if (headerHits == headerBits){
            digitalWrite(ledPin, 1);
          }
        }
        else{
          add(bitState);
        }
      }
      else{
        if(headerHits < headerBits){
          noErrors = false;
        }
        else{
          if ((!firstZero) && (headerHits >= headerBits)){
            firstZero = true;
          }
          add(bitState);
        }
      }
    }
  }
  digitalWrite(ledPin, 0);
}

void add(byte bitData){
  if (discNos > 0){ 
    discNos--;
  }
  else{ //add bit into byte
    dataByte = (dataByte << 1) | bitData;
    nosBits++;
    if (nosBits == 8){ //add byte into bank
      nosBits = 0;
      manchester[nosBytes] = dataByte;
      nosBytes++;
    }
    if (nosBytes == maxBytes){
      hexBinDump();
      nosBytes = 0;
      eraseManchester();
    }
  }
}

void hexBinDump(){
  Serial.print("D ");
  for(int i = 0; i < maxBytes; i++){ 
    byte mask = B10000000;
    if (manchester[i] < 16){
      Serial.print("0");
    }
    Serial.print(manchester[i], HEX);
    Serial.print(" ");
    for (int k = 0; k < 8; k++){
      if (manchester[i] & mask){
        Serial.print("1");
      }
      else{
        Serial.print("0");
      }
      mask = mask >> 1;
    }
    Serial.print(" ");
  }
  Serial.println();
}

void eraseManchester(){
  for(int i=0; i < 20; i++){ 
    manchester[i] = 0;
  }
}

const int txLed = 8;
const int debugLed = LED_BUILTIN;
const int REPEAT_NUM = 1;

byte thisBit; // the current data bit polarity

byte dataBuffer[8];
int dataLength = 8;

word sDelay = 300; // 1/2 of bit pattern duration, in uS

void setup(){
  pinMode(txLed, OUTPUT);
  pinMode(debugLed, OUTPUT);

  dataBuffer[0] = 0xFA;
  dataBuffer[1] = 0xCE;
  dataBuffer[2] = 0xDE;
  dataBuffer[3] = 0xAD;
  dataBuffer[4] = 0xBE;
  dataBuffer[5] = 0xEF;
  dataBuffer[6] = 0x00;
  dataBuffer[7] = 0xFF;
} 

void loop(){
  digitalWrite(debugLed, HIGH);
  //just to send differrent messages
  dataBuffer[6]++;
  dataBuffer[7]--;
  
  //repeat REPEAT_NUM times 
  for (int packet = 0; packet < REPEAT_NUM; packet++){ 
    digitalWrite(txLed, 1);

    // Header (set of ones)
    for (int j = 0; j < 20; j++) {
      sendBit(1);
    }
    // Header end
    sendBit(0);
    
    for (byte bytePointer = 0; bytePointer < dataLength; bytePointer++){
      byte mask = B10000000; //MSB -> LSB
      for (int k = 0; k < 8; k++){
        thisBit =  mask & dataBuffer[bytePointer];
        sendBit(thisBit);
        mask = mask >> 1;
      }
    }
    digitalWrite(txLed, 1);

    //break between repeating packets
    delay(20);
  }
  digitalWrite(debugLed, LOW);
  
  // Wait for 20 Seconds before next transmission
  for (int second = 0; second < 20; second++){
    delay(1000);
  }
}

void sendBit(bool value){
  // zero = high -> low
  // one  = low -> high
  digitalWrite(txLed, !value);
  delayMicroseconds(sDelay);
  digitalWrite(txLed, value);
  delayMicroseconds(sDelay);
}

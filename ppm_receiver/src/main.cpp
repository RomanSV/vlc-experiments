#include <Arduino.h>

#include "PPMDecoder.h"

constexpr int LIGHT_CHANGE_THRESHOLD = 10;
constexpr int REFERENCE_VOLTAGE_PIN = 3;

byte channelAmount = 1;
PPMDecoder ppmDecoder(channelAmount);

void setup() {
    pinMode(REFERENCE_VOLTAGE_PIN, OUTPUT);
    analogWrite(REFERENCE_VOLTAGE_PIN, map(analogRead(A1), 0, 1023, 0, 255) + LIGHT_CHANGE_THRESHOLD);

    // Setup analog comparator registers
    ADCSRA &= ~(1 << ADEN); // Disable ADC
    ADCSRB |= (1 << ACME);  // Enable multiplexer for the negative comparator input
    ADMUX |= (1 << MUX0);   // MUX = 001 (A1)
    ACSR |= (1 << ACIE);    // Enable analog comparator interrupt
    ACSR |= (1 << ACIS1);   // Interrupt mode: on falling edge (when the light turns on)
    ACSR &= ~(1 << ACIS0);

    Serial.begin(115200);
}

void loop() {
    for (byte channel = 1; channel <= channelAmount; channel++) {
        Serial.print(floor((ppmDecoder.getLastValue(channel) - 200) / 16.0));
        if (channel < channelAmount) Serial.print('\t');
    }
    Serial.println();
    delay(20);
}

ISR(ANALOG_COMP_vect) {
    ppmDecoder.handlePulse();
}
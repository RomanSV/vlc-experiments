#ifndef PPM_RECEIVER_PPM_READER_H
#define PPM_RECEIVER_PPM_READER_H

#include <Arduino.h>

class PPMDecoder {

public:

    PPMDecoder(byte channelAmount, unsigned long minValue = 200, unsigned long maxValue = 1224);

    ~PPMDecoder();

    unsigned long getLastValue(byte channel);

    void handlePulse();

private:
    const unsigned long minValue;
    const unsigned long maxValue;

    const unsigned long minimumInterframeGap = 100;
    const unsigned long channelValueMaxError = 10;

    byte channelAmount = 0;

    volatile unsigned long *receivedValues = nullptr;

    volatile byte pulseCounter = 0;

    volatile unsigned long microsAtLastPulse = 0;
};


#endif //PPM_RECEIVER_PPM_READER_H

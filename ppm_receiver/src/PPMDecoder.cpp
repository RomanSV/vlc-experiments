#include "PPMDecoder.h"

PPMDecoder::PPMDecoder(
        byte channelAmount, unsigned long minValue, unsigned long maxValue
) : minValue(minValue), maxValue(maxValue) {
    if (channelAmount > 0) {
        this->channelAmount = channelAmount;
        receivedValues = new unsigned long[channelAmount]();
    }
}

PPMDecoder::~PPMDecoder() {
    delete[] receivedValues;
}

void PPMDecoder::handlePulse() {
    unsigned long previousPulse = microsAtLastPulse;
    microsAtLastPulse = micros();
    unsigned long value = microsAtLastPulse - previousPulse;

    if (value > maxValue + minimumInterframeGap) {
        // Reset the pulse counter on interframe gap
        pulseCounter = 0;
    } else {
        if (pulseCounter < channelAmount) {
            if (value >= minValue - channelValueMaxError && value <= maxValue + channelValueMaxError) {
                receivedValues[pulseCounter] = constrain(value, minValue, maxValue);
            }
        }
        pulseCounter++;
    }
}

unsigned long PPMDecoder::getLastValue(byte channel) {
    unsigned long value;
    noInterrupts();
    value = receivedValues[channel - 1];
    interrupts();
    return value;
}
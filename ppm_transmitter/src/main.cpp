#include <Arduino.h>
#include "PPMEncoder.h"


#define OUTPUT_PIN 10

unsigned long timing;

byte data = 0;
PPMEncoder<1> ppmEncoder{200, 1224, 10000, 50};

ISR(TIMER1_COMPA_vect) {
    ppmEncoder.interrupt();
}

void setup() {
    ppmEncoder.begin(OUTPUT_PIN);
    Serial.begin(115200);
}

void loop() {
    if (millis() - timing > 500) {
        timing = millis();
        ppmEncoder.setChannel(0, (data * 4) + 200);
        data += 4;
    }
}